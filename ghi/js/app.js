function createCard(name, description, pictureUrl, start, end, location) {
    return `
            <div class="card shadow-sm p-3 mb-5 bg-body-tertiary rounded text-white bg-dark mb-3 " style="margin-bottom: 20px;">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</small>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                <small class="text-muted">${start} - ${end}</small>
                </div>
            </div>
        `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error("Uh-oh no conferences")
    } else {
      const data = await response.json();
      let columnCount = 0;

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const start = new Date(details.conference.starts).toLocaleDateString();
          const end = new Date(details.conference.ends).toLocaleDateString();
          const location = details.conference.location.name;
          const html = createCard(title, description, pictureUrl, start, end, location);
          const column = document.querySelector(`#col-${columnCount}`);
          column.innerHTML += html;
          columnCount += 1;
          if (columnCount > 2) {
            columnCount = 0;
          }
        }
      }

    }
  } catch (e) {
    const errorElement = document.createElement('div')
    const error = raiseError(e)
    const column = querySelector('.col')
    column.appendChild(errorElement)
  }

});
